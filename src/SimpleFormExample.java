import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javafx.application.Application;
import javafx.stage.Stage;

public class SimpleFormExample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// 1. Create & configure user interface controls
		// -----------------------------------------------

		// name label -test
		Label nameLabel = new Label("Enter your name");

		// name textbox
		TextField nameTextBox = new TextField();

		// button
		Button goButton = new Button();
		goButton.setText("Click me!");

		goButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				// Logic for what should happen when you push buttondas
				// System.out.println("Hello World");
				// ---------------------------------------
				// Expected output:
				// When person presses button, output HELLO ____
				// ____ = whatever they typed in the box
				// ---------------------------------------

				// 1. Get whatever they typed in the box
				String name = nameTextBox.getText();

				// 2. Display "HELLO ____" in the console

				System.out.println("Hello  " + name);

				// 3. clear textbox
				nameTextBox.setText("");

			}
		});
		// 2. Make a layout manager
		// StackPane root = new StackPane();
		// HBox root = new HBox();
		VBox root = new VBox();
		root.setSpacing(10); // increase the space between UI controls

		// 3. Add controls to the layout manager
		root.getChildren().add(nameLabel);
		root.getChildren().add(nameTextBox);
		root.getChildren().add(goButton);

		// 4. Add layout manager to scene
		// 5. Add scene to a stage

		// set the height & width of app to (300 x 250);
		primaryStage.setScene(new Scene(root, 300, 250));

		// setting the title bar of the app
		primaryStage.setTitle("Example 01");

		// 6. Show the app
		primaryStage.show();

	}

}
