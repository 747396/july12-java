import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class WageCalculator extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		// 1. Create & configure user interface controls
		
		// name
		Label nameLabel = new Label("Enter Name");
		TextField nameTextBox = new TextField();
		
		// hours worked
		Label hoursWorkedLabel = new Label("Enter Hours Worked");
		TextField hoursWorkedTextBox = new TextField();
		
		// hourly wage
		Label wageLabel = new Label("Enter Hourly Wage");
		TextField wageTextBox = new TextField();

		// calculate button
		Button btn = new Button();
		btn.setText("Calculate!");
		
		//Add event handler for button

		// results
		Label resultsLabel = new Label("");
		
		btn.setOnAction(new EventHandler<ActionEvent>() {
		  
			@Override
		    public void handle(ActionEvent e) {
		        // Logic for what should happen when you push button
               
		    	System.out.println("Click Button");
           
		    	// 1. Get what the person typed in the text boxes
	    		String name = nameTextBox.getText();
	    		String hoursWorked = hoursWorkedTextBox.getText();
	    		String hourlyRate = wageTextBox.getText();
	    		
	    		// test that you got all the data correctly
	    		System.out.println(name);
	    		System.out.println(hoursWorked);
	    		System.out.println(hourlyRate);
	    		
	    		// 2. Do some math (calculation)
	    		
	    		// 2a. Convert string input to a doubule
	    		// Double.parseDouble() = built in Java function to convert String --> double
	    		double hours = Double.parseDouble(hoursWorked);
	    		double rate = Double.parseDouble(hourlyRate);
	    		// 2b. Do the math
	    		double wages = hours * rate;
	    		
	    		// 2c. Test 
	    		System.out.println("Result: " + wages);
	    	
	    		// 3. Output the result to the screen
           resultsLabel.setText(name + " earns $ " + wages);
		    }
		});


		
		
		// 2. Make a layout manager
		VBox root = new VBox();
	
		// 3. Add controls to the layout manager
		root.getChildren().add(nameLabel);
		root.getChildren().add(nameTextBox);
		root.getChildren().add(hoursWorkedLabel);
		root.getChildren().add(hoursWorkedTextBox);
		root.getChildren().add(wageLabel);
		root.getChildren().add(wageTextBox);
		root.getChildren().add(btn);
		root.getChildren().add(resultsLabel);
		
		
		
		// 4. Add layout manager to scene
		// 5. Add scene to a stage
		primaryStage.setScene(new Scene(root, 300, 250));
		
		// 6. Show the app
		primaryStage.show();
	}
}
